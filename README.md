# BioView Metadata API
This API delivers image information and annotation data, such as regions of interest and meta data 
to the main application: BioView Image Library.

## Getting Started

```
git clone https://Whreitsma@bitbucket.org/Whreitsma/bioview-metadata-api.git
```
Clone the project and run it with Intellij and Java version 8

### Prerequisites

This API is an add-on requirement for running the BioView Image Library. but could still run without it.

```
Clone project: git clone https://bitbucket.org/JoukeProfijt/image-library/src/master/
Run this project in Intellij on port 8081 and de main project on 8080
```
If you want to run the program solo, you can run it on port 8080.

### Installing

Clone Project with git:
```
git clone https://Whreitsma@bitbucket.org/Whreitsma/bioview-metadata-api.git
```
open project with gradle using intellij.
Select Java version 8 as language. 

## Deployment

Use Gradle to build a jar file, which can be run by JRE(Java Runtime Enviroment)

## Built With

* [Gradle](https://gradle.org/) - Dependency Management
* [Spring](https://spring.io/) - Java Web framework
* [Asciidoctor](https://Asciidoctor.org/) - automatic text processor

## Authors

* **Jouke Profijt** - [JoukeProfijt](https://bitbucket.org/JoukeProfijt/)
* **Kim Chau Duong** - [kcduong](https://bitbucket.org/kcduong/)
* **Wietse Reitsma** - [Whreitsma](https://bitbucket.org/Whreitsma/)


## Acknowledgments

* Marcel Kempenaar - Teaching us the basics of micro services

